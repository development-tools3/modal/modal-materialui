import React from 'react';
import ReactDOM from 'react-dom/client';
import { RouterProvider } from 'react-router-dom';
import { router } from 'src/router/router.js';
import { BrowserRouter as Router } from 'react-router-dom';

import { theme } from 'src/styles/_materialTheme.js';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { App } from './components/App';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <RouterProvider router={router} />
    {/* <Router>
      <App />
    </Router> */}
  </ThemeProvider>
);
