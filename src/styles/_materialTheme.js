import { createTheme } from '@mui/material';
import { orange, purple } from '@mui/material/colors';

export const theme = createTheme({
  palette: {
    primary: { main: purple[500] },
    secondary: {
      main: orange[500],
    },
    transparent: {
      main: '#eff3f4',
      contrastText: '#000',
    },
    gray: {
      main: '#eff3f4',
      contrastText: '#fff',
    },
    black: {
      main: '#000',
      contrastText: '#fff',
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: '20px',
          textTransform: 'none',
        },
      },
      variants: [
        {
          props: { color: 'transparent' },
          style: {
            background: 'transparent',
            color: '#fff',
            border: '1px solid #fff',
            '&:hover': {
              backgroundColor: '#ffffff33',
              border: '1px solid #ffffff85',
            },
          },
        },
        {
          props: { color: 'gray' },
          style: {
            backgroundColor: '#eff3f4',
            border: '1px solid transparent',
            color: '#000',
            '&:hover': {
              backgroundColor: '#d3d3d3f5',
              border: '1px solid transparent',
            },
          },
        },
        {
          props: { color: 'black' },
          style: {
            backgroundColor: '#000',
            border: '1px solid transparent',
            color: '#fff',
            '&:hover': {
              backgroundColor: '#777',
              border: '1px solid transparent',
            },
          },
        },
      ],
    },
  },
});
