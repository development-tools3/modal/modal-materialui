import {
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
} from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';
import { useState } from 'react';

export const Modal = ({ children = null }) => {
  // set state modal for transition close the modal when browser change back url
  const [isOpen, setIsOpen] = useState(true);

  const navigate = useNavigate();
  const location = useLocation();

  const handleClose = async () => {
    // for transition close modal
    setIsOpen(false);
    await new Promise((resolve) => {
      setTimeout(resolve, 300);
    });

    // change back url
    !!location.state ? navigate(-1) : navigate('/');
  };

  return (
    <>
      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle>
          Modal
          <Button onClick={handleClose}>Close</Button>
        </DialogTitle>
        <Divider />
        OnlyModal
        <DialogContent>{children}</DialogContent>
      </Dialog>
    </>
  );
};
