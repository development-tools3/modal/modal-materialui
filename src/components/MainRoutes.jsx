import { useRoutes } from 'react-router-dom';
import { mainRoutes } from 'src/router/routes';

export const MainRoutes = ({ location }) => {
  return useRoutes(mainRoutes(), location);
};
