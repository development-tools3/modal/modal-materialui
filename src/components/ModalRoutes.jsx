import { useRoutes } from 'react-router-dom';
import { modalRoutes } from 'src/router/routes';

export const ModalRoutes = () => {
  return useRoutes(modalRoutes());
};
