import { AppBar, Box, Button, Link, Toolbar } from '@mui/material';
import { NavLink, Outlet, useLocation, useNavigate } from 'react-router-dom';
import { MainRoutes } from './MainRoutes';
import { ModalRoutes } from './ModalRoutes';

export const Layout = () => {
  const location = useLocation();
  const background = location.state && location.state.background;

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" color="primary">
        <Toolbar>
          <Link
            sx={{ mr: 1 }}
            color="secondary"
            component={NavLink}
            to="/"
            underline="none"
          >
            Home
          </Link>
          <Link
            sx={{ mr: 1 }}
            color="secondary"
            component={NavLink}
            to="/news"
            underline="none"
          >
            News
          </Link>
          <Link
            sx={{ mr: 1 }}
            color="secondary"
            component={NavLink}
            to="/user"
            underline="none"
          >
            User
          </Link>
          <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
          {/* Login form */}
          <Link
            sx={{ mr: 1 }}
            color="secondary"
            component={NavLink}
            to="/modal/login"
            state={{ background: location }}
            underline="none"
          >
            login
          </Link>

          {/* Register form */}
          <Link
            sx={{ mr: 1 }}
            color="secondary"
            component={NavLink}
            to="/modal/register"
            state={{ background: location }}
            underline="none"
          >
            register
          </Link>
        </Toolbar>
      </AppBar>
      <Toolbar>
        <MainRoutes location={background || location} />
      </Toolbar>
      <ModalRoutes />
    </Box>
  );
};
