import { Home } from 'src/pages/Home';
import { News } from 'src/pages/News';
import { User } from 'src/pages/User';
import { LoginForm } from 'src/pages/LoginForm';
import { RegisterForm } from 'src/pages/RegisterForm';

export const mainRoutes = () => {
  return [
    {
      path: '/',
      element: <Home />,
    },
    {
      path: '/news',
      element: <News />,
    },
    {
      path: '/user',
      element: <User />,
    },
  ];
};

export const modalRoutes = () => {
  return [
    {
      path: '/modal/login',
      element: <LoginForm />,
    },
    {
      path: '/modal/register',
      element: <RegisterForm />,
    },
  ];
};
