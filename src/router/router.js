import { createBrowserRouter } from 'react-router-dom';
import { Home } from 'src/pages/Home';
import { News } from 'src/pages/News';
import { User } from 'src/pages/User';
import { App } from 'src/components/App';

import { LoginForm } from 'src/pages/LoginForm';
import { RegisterForm } from 'src/pages/RegisterForm';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: 'error',
    children: [
      {
        path: '/',
        element: <Home />,
        index: true,
      },
      {
        path: '/news',
        element: <News />,
      },
      {
        path: '/user',
        element: <User />,
      },
      {
        path: '/modal/login',
        element: <LoginForm />,
      },
      {
        path: '/modal/register',
        element: <RegisterForm />,
      },
    ],
  },
]);
